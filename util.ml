(* -*- Mode:caml; -*-
   *===----------------------------------------------------------------------===
   * Version: $Id: util.ml,v 0.0 2012/03/31 13:41:36 bobzhang1988 Exp $
   *===----------------------------------------------------------------------===*)
open Format
open Unix

let (|>) x f =  f x
let (^$) f x = f x

(* exceptions *)
exception Bad_parse
exception Found

let finally handler f x =
  let r = (
    try f x
    with e -> handler () ; raise e
   ) in
  handler ();
  r

(** range of integers *)
let rec range i j =
  if i > j then
    []
  else i :: (range (i+1) j)

let prerr_endlinef fmt = ksprintf prerr_endline  fmt

(** all capital, then is command *)
let is_cmd s =
  let n = String.length s in 
  if n = 0 then
    false
  else
    let b = ref true in
    let i = ref 0 in 
    while !b && !i < n do begin 
      if not (s.[!i] >= 'A' && s.[!i] <='Z') then
	b:=false;
      incr i;
    end 
    done;
    !b
let (|-) f g  x = g (f x)
let print_string_list  = List.iter (print_string |- print_newline)
let option_get = function
  | Some x -> x
  | None -> invalid_arg "option_get"

let fatalf fmt =
  ksprintf (fun str ->
    eprintf "Fatal error: %s !\n%!" str;
    exit 1
    ) fmt


let str_of_time ({tm_sec; tm_min; tm_hour; _ } ) =
  sprintf "%02d:%02d:%02d" tm_hour tm_min tm_sec

(** TODO we should put this somewhere better *)
let log_printf ?(debug=true)  format =
  ksprintf (fun str ->
    if debug then begin
      prerr_string (sprintf "%s)) " (str_of_time (localtime (gettimeofday()))));
      prerr_endline str
    end)  format

(** turn a pretty printer function to a string_of_function
 *)
let make_of_string pretty content =
  let buf = Buffer.create 100 in
  let fmt = Format.formatter_of_buffer buf in begin
    pretty fmt content;
    Format.pp_print_flush fmt  ();
    Buffer.contents buf
  end

(** first parse as digit, if failed
    will try to gethostbyname
    gethostbyname "www.google.com" works
 *)
let sockaddr_of_name_port  (s,port) =
  try
    ADDR_INET (inet_addr_of_string s, port)
  with
    e ->
      try
        ADDR_INET ((gethostbyname s).h_addr_list.(0),port)
      with
        e -> invalid_arg "sockaddr_of_string_int"

let print_usage () = begin
  log_printf "Usage: dchat < nickname > [ server_ip:server_port ]@.";
  exit 1
end

(** parse_args
    dchar nick ip:port
    dchat nick
 *)
let parse_args () =
  try match Array.length Sys.argv with
  | 3 -> begin 
      match Str.(split  (regexp ":") Sys.argv.(2)) with
      | [ip;port] -> begin 
          if int_of_string port > 65535 then
            raise Bad_parse
          else
            (Sys.argv.(1), Some (sockaddr_of_name_port (ip, int_of_string port)))
      end
      | _ -> assert false
  end 
  | 2 -> (Sys.argv.(1), None)
  | _ -> raise Bad_parse
  with e -> begin print_usage () end

