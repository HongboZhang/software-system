dsl:
	ocamlbuild -pp  'camlp4of' -cflags -I,+camlp4 -lflags -I,+camlp4,dynlink.cma,camlp4lib.cma,str.cma,unix.cma DSL_util.byte

clean :
	ocamlbuild -clean
	rm -rf *.log
