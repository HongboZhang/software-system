open Format
open Unix  
open Util
let (|>) x f =  f x
let prerr_endlinef fmt = ksprintf prerr_endline  fmt    
  
let roles = Hashtbl.create 50
let col = Str.regexp ":"
let ws = Str.regexp "[ \t]+"    
(** make sure dchat is in your search path*)

let dchat = begin
  let chan = Unix.open_process_in "which dchat" in
  let ans =input_line chan  in
  if Sys.file_exists ans then begin 
    prerr_endlinef "dchat:%s" ans;
    ans
  end 
  else begin
    prerr_endlinef "dchat not found: exit";
    exit 1;
  end 
end 
let machine_id = ref 1

let config = begin
  let conf = open_in "config.in" in
  let pairs = ref [] in 
  finally (fun _ -> close_in conf) (fun conf ->
    try
      while true do
        let line = (input_line conf) in
        let () = prerr_endline line in 
        let [a;b] = Str.bounded_split col line 2 in
        pairs := (a,b):: !pairs
      done
    with
      End_of_file -> ()) conf;
  !pairs
end

let client_port  =
  try
    List.assoc "client_port" config
  with Not_found -> begin
    prerr_endline "client_port not found";
    raise Not_found
  end

let pennid =
  try
    List.assoc "pennid" config
  with Not_found -> begin
    prerr_endline "pennid not found";
    raise Not_found
  end 

let gen_command i pennid =
  sprintf "ssh spec%02d ps -U %s | grep dchat | sed 's/\\W*//' | cut -d ' ' \
    -f 1 | while read ss; do echo \"spec%02d $ss\"; ssh spec%02d kill $ss; \
    done" i pennid i i

(** kill your dchat programs
    to save your udp port 
 *)      
let reset ?(from=1) ?(until = !machine_id) ()= begin
  for i = from to until do 
    gen_command i pennid|> Sys.command |> ignore
  done ;
  Hashtbl.clear roles;
  machine_id := 1;
end 
    
let create_and_redir (name,args,fnormal,ferr) =
  let (fd_in,fd_out) = pipe() in
  match fork () with
  | 0 -> begin 
      dup2 fd_in stdin;
      close fd_in;
      close fd_out;
      let out_chan = open_out fnormal in
      let err_chan = open_out ferr in  begin
        prerr_endlinef "create file %s and %s " fnormal ferr;
        dup2 (descr_of_out_channel out_chan) stdout;
        dup2 (descr_of_out_channel err_chan) stderr;
        execvp name args
      end 
  end
  | pid -> begin
      close fd_in;
      (fd_out,pid)
  end 

(** without buffering *)
let write_out fd_out str =
  write fd_out str 0 (String.length str)

let role_exists role =  Hashtbl.mem roles role


  
let kill role =
  if role_exists role then 
    let _,pid,machine = Hashtbl.find roles role in begin
      Unix.kill pid 9 
    end
  else
    prerr_endlinef "role%s not exist" role
      

    
let add_client role other  =
  if role_exists role then
    prerr_endlinef "role:%s already exists" role
  else if not (role_exists other) then
    prerr_endlinef "role:%s not exists" other
  else
    
    let machine = sprintf "spec%02d" !machine_id in
    let () = incr machine_id in    
    let _,_,m = Hashtbl.find roles other in
    let m = string_of_inet_addr(gethostbyname m).h_addr_list.(0) in
    let args = [|"ssh"; machine; dchat; role; m^":"^ client_port|] in 
    let (fid,pid) = create_and_redir ("ssh",args,role^".log",role^".err.log") in
    let () = prerr_endlinef "%s" (Array.fold_left (fun x y -> x ^ " " ^ y) "" args) in
    Hashtbl.add roles role (fid,pid,machine)

let add_server role =
  if role_exists role then
    prerr_endlinef "role:%s already exists" role
  else 
    let machine = sprintf "spec%02d" !machine_id in
    let args = [|"ssh"; machine; dchat; role|] in  (* only nick *)
    let () = incr machine_id in
    let (fid,pid) = create_and_redir ("ssh",args,role^".log",role^".err.log") in
    let () = prerr_endlinef "%s" (Array.fold_left (fun x y -> x ^ " " ^ y) "" args) in
    Hashtbl.add roles role (fid,pid,machine)


let send_string role str=
  if role_exists role then 
    let fd_out,_,_ = Hashtbl.find roles role in begin
      let _ = write_out fd_out (str^"\n") in
      prerr_endlinef "%s:%s\n" role (str^"\n")
    end
  else prerr_endlinef "role%s does not exists" role


open Camlp4.PreCast
  
let parser_of_entry entry  s =
  try Gram.parse entry (Loc.mk "<string>") (Stream.of_string  s)
  with
    Loc.Exc_located(loc, e) -> begin 
      prerr_endline (Loc.to_string loc);
      let start_bol,stop_bol,
        start_off, stop_off =
        Loc.(start_bol loc,
             stop_bol loc,
             start_off loc,
             stop_off loc
            ) in
      let abs_start_off = start_bol + start_off in
      let abs_stop_off = stop_bol + stop_off in
      let err_location = String.sub s abs_start_off
          (abs_stop_off - abs_start_off + 1) in
      prerr_endline (sprintf "err: ^%s^" err_location);
      raise e ;
    end
        


let instruction = Gram.Entry.mk "instruction"
let instruction_eoi = Gram.Entry.mk "instruction_eoi"

let _ = begin
  EXTEND Gram GLOBAL: instruction instruction_eoi;
  instruction_eoi:
    [ [x = instruction ; `EOI -> x ] ];
  instruction:
    [
     "top"
       [ x=name ; "via"; y=name  -> begin
         prerr_endlinef "add_client %s %s" x y;
         add_client x y;
       end
       | x=name ; ":"; s=STRING -> begin
           prerr_endlinef "%s:\"%s\"" x s ;
           send_string x  s;
       end 
       | x=name ; "crash" -> begin
           prerr_endlinef "kill %s" x ;
           kill x;
       end
       | x=name ; "create" -> begin
           prerr_endlinef "create server %s" x ;
           add_server x;
       end 
       | "time" ; `INT(x,_) ->  begin
           prerr_endlinef "sleep %d" x ;
           Unix.sleep x;
       end 
       | "(" ; names = LIST1 name; ")" ; `INT(n,_);
         "interval"; `FLOAT(f,_)-> begin
           let () =
             prerr_endlinef "(%s) in turn interval %f"
               (List.fold_left  (fun x y -> x ^ " " ^ y) "" names) f in  
           let start =  ref 0 in
           for i = 1 to n do 
             List.iter (fun role -> begin
               send_string role (string_of_int !start);
               Unix.select [] [] [] f |> ignore ;
               incr start;
             end
              ) names
           done 
           end 
       | -> begin
           prerr_endlinef "empty";
       end]];
   name:
    [ [x=LIDENT -> x
      |x=UIDENT -> x]];
  END;
end
    
let parse_instruction = parser_of_entry instruction
let parse_instruction_eoi = parser_of_entry instruction_eoi


let file_lines_of str =
  let chan = open_in str in
  let lines = ref [] in
  (try
    while true do
      lines := input_line chan :: !lines
    done;
  with
    End_of_file -> begin 
      close_in chan;
    end);
  List.rev !lines
    
(* let lines =  "leader_elect.ml" |> file_lines_of *)
(* let ()  = reset ~until:3 () *)

let _ =

  let () = reset ~until:5 () in
  let file_name = Sys.argv.(1) in
  if Sys.file_exists file_name then 
    let chan = open_in file_name in
    try      
      while true do 
        (* List.iter parse_instruction_eoi lines *)
        let line = input_line chan in 
        let ()  =  prerr_endline line in
        parse_instruction_eoi line
      done
    with
      End_of_file ->
        prerr_endlinef "finished"
  else 
    prerr_endlinef "file %snot exists" file_name
